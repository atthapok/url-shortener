'use strict';

const express = require('express'),
    router = express.Router(),
    common = require('../middlewares/common'),
    validator = require('../middlewares/validator'),
    urlShortenController = require('../controllers/url-shorten.controller');


router.get('/:code', validator.getUrlShorten, urlShortenController.getUrlShorten, common.handleResponse);
router.post('/api/v1/url-shorten', validator.createUrlShorten, urlShortenController.createUrlShorten, common.handleResponse);

module.exports = router;

