const utils = require('../helpers/utils'),
    UrlShorten = require('../models/url-shorten.model'),
    cfg = require('../config/config'),
    shortid = require("shortid"),
    ObjectId = require('mongoose').Types.ObjectId,
    resMsg = require('../helpers/message');

const getUrlShorten = async (req, res, next) => {
    const command = 'getUrlShorten',
        startTime = utils.getTimeInMsec();
    try {
        const doc = await UrlShorten.model.findOne({urlCode: req.params.code}).select(UrlShorten.selectField).lean();
        if (!doc) {
            req.response = utils.setResponse(command, startTime, 404, resMsg.getMsg(40400, req.get("lang")), null);
            next();
        } else {
            res.redirect(doc.originalUrl);
        }
    } catch (err) {
        req.response = utils.setResponse(command, startTime, 500, resMsg.getMsg(50000, req.get("lang")), err);
        next();
    }
};

const createUrlShorten = async (req, res, next) => {
    const command = 'createUrlShorten',
        startTime = utils.getTimeInMsec();
    try {
        const resData = resMsg.getMsg(20000, req.get("lang"));
        let query = {};
        if (req.body.alias){
            query.urlCode = req.body.alias;
        } else {
            query.originalUrl = req.body.originalUrl;
        }
        const doc = await UrlShorten.model.findOne(query).select(UrlShorten.selectField).lean();
        if (doc) {
            resData.data = doc;
            req.response = utils.setResponse(command, startTime, 200, resData, null);
            next();
        } else {
            const data = {
                _id: new ObjectId(),
                originalUrl: req.body.originalUrl,
                urlCode: (req.body.alias) ? req.body.alias : shortid.generate()
            };
            data.shortUrl = cfg.domain + data.urlCode;
            let obj = new UrlShorten.model(data);
            await obj.save();
            resData.data = data;
            req.response = utils.setResponse(command, startTime, 200, resData, null);
            next();
        }
    } catch (err) {
        req.response = utils.setResponse(command, startTime, 500, resMsg.getMsg(50000, req.get("lang")), err);
        next();
    }
};

module.exports = {
    createUrlShorten,
    getUrlShorten
};