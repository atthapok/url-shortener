'use strict';

const resMsg = require('../helpers/message'),
    logger = require('../helpers/logger'),
    validUrl = require('valid-url'),
    utils = require('../helpers/utils');

const getUrlShorten = (req, res, next) => {
    let command = 'getUrlShorten',
        startTime = utils.getTimeInMsec(),
        developerMessage = '',
        flag = true;
    req.checkParams('code', 'code is required').notEmpty();
    validateRequest(req, res, next, startTime, command, flag, developerMessage);
};

const createUrlShorten = (req, res, next) => {
    let command = 'createUrlShorten',
        startTime = utils.getTimeInMsec(),
        developerMessage = '',
        flag = true;

    req.checkBody('originalUrl', 'originalUrl is required').notEmpty();
    if (req.body.originalUrl && !validUrl.isUri(req.body.originalUrl)){
        flag = false;
        developerMessage = 'Invalid originalUrl'
    }
    validateRequest(req, res, next, startTime, command, flag, developerMessage);
};

const validateRequest = (req, res, next, startTime, command, flag, message) => {
    let errors = req.validationErrors();
    if (errors || !flag) {
        let resData = resMsg.getMsg(40000, req.get("lang"));
        if (process.env.NODE_ENV !== 'production') {
            resData.developMessage = (errors) ? JSON.stringify(errors) : message;
        }

        console.log(resData);
        res.status(400).json(resData);
        logger.writeLog(command, req, startTime, resMsg.getMsg(40000, req.get("lang")), JSON.stringify(errors));
    } else {
        next();
    }
};

module.exports = {
    createUrlShorten,
    getUrlShorten
};

