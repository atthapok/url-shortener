'use strict';

const logger = require('../helpers/logger');

let handleResponse = (req, res) => {
    logger.writeLog(req.response.command, req, req.response.startTime, req.response.data, req.response.err);
    return res.status(req.response.status).json(req.response.data);
};

module.exports = {handleResponse};