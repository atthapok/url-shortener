'use strict';

const express = require('express'),
    bodyParser = require('body-parser'),
    nocache = require('nocache'),
    nosniff = require('dont-sniff-mimetype'),
    frameguard = require('frameguard'),
    xssFilter = require('x-xss-protection'),
    ienoopen = require('ienoopen'),
    validator = require('express-validator'),
    serveStatic = require('serve-static'),
    compression = require('compression'),
    morgan = require('morgan'),
    h5bp = require('h5bp'),
    api = require('../routes/api'),
    resMsg = require('../helpers/message'),
    utils = require('../helpers/utils'),
    logger = require('../helpers/logger'),
    cfg = require('../config/config'),
    app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false, limit: '10mb', parameterLimit: 1000000}));
if (process.env.NODE_ENV === 'production') {
    app.use(compression());
} else {
    app.use(morgan('dev'));
}

app.disable('x-powered-by');
app.use(ienoopen());
app.use(nocache());
app.use(nosniff());
app.use(frameguard());
app.use(xssFilter());
app.use(validator());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header(
        "Access-Control-Allow-Headers",
        "Content-type,Accept,x-access-token,X-Key"
    );
    if (req.method === "OPTIONS") {
        res.status(200).end();
    } else {
        next();
    }
});

function ensureSecure(req, res, next) {
    if (req.secure) {
        // OK, continue
        return next();
    }
    res.redirect('https://' + req.hostname + req.url); // express 4.x
}

if (cfg.server.protocol === 'https') {
    app.all('*', ensureSecure);
}

app.use('', api);

app.use(h5bp({ root: __dirname + '/dist' }));
app.use(express.static('./public', {
    maxage: '24h'
}));

app.use(serveStatic('public', {'index': ['index.html', 'index.htm']}));

// /*----- Catch 500 Error -----*/
app.use(function (err, req, res, next) {
    let error = resMsg.getMsg(50000, req.get("lang"));
    res.status(500).json(error);
    error.url = req.url;
    logger.writeLog('Client Request', req, utils.getTimeInMsec(), error, err);
});

module.exports = app;
