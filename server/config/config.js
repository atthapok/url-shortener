'use strict';

const join = require('path').join,
    processEnv = (process.env.NODE_ENV) ? process.env.NODE_ENV : 'development';

console.log("processEnv: " + processEnv);

const env = {
    development: {
        host: {protocol: 'https', ip: '127.0.0.1', port: 443},
        mongodbUri: 'mongodb://digital2018:63e99f11e8b5a72a5f0c892e8a4d9d140ddf2c8d47db375179136126fde23706@13.229.131.241:27017/digital-supply-chain',
        debug: true
    },
    test: {
        host: {protocol: 'https', ip: '127.0.0.1', port: 443},
        mongodbUri: 'mongodb://digital2018:63e99f11e8b5a72a5f0c892e8a4d9d140ddf2c8d47db375179136126fde23706@13.229.131.241:27017/digital-supply-chain',
        debug: true
    },
    staging: {
        host: {protocol: 'https', ip: '127.0.0.1', port: 443},
        mongodbUri: 'mongodb://digital2018:63e99f11e8b5a72a5f0c892e8a4d9d140ddf2c8d47db375179136126fde23706@127.0.0.1:27017/digital-supply-chain',
        debug: true
    },
    production: {
        host: {protocol: 'https', ip: '127.0.0.1', port: 443},
        mongodbUri: 'mongodb://digital2018:63e99f11e8b5a72a5f0c892e8a4d9d140ddf2c8d47db375179136126fde23706@54.255.175.160:27017/digital-supply-chain',
        debug: false
    }
};

module.exports = {
    server: {
        protocol: env[processEnv].host.protocol,
        ip: env[processEnv].host.ip,
        port: env[processEnv].host.port,
        cert: join(__dirname, '../../certs/cert.pem'),
        key: join(__dirname, '../../certs/key.pem'),
    },
    mongodb: {
        uri: env[processEnv].mongodbUri,
        serverOpt: {
            noDelay: true,
            keepAlive: 1,
            connectTimeoutMS: 10000,
            socketTimeoutMS: 10000,
            poolSize: 5,
            // useMongoClient: true,
            useNewUrlParser: true,
            native_parser: false,
            useFindAndModify: false
        },
        retryTime: 5000
    },
    log: {
        debug: env[processEnv].debug,
        error: true,
        rotateTime: 15,
        path: join(__dirname, '../../logs/'),
        periodClearLog: 7
    },
    localize: {
        en: join(__dirname, '../resource/locale-en.json'),
        th: join(__dirname, '../resource/locale-th.json'),
        default: 'en'
    },
    domain: env[processEnv].host.protocol + "://" + env[processEnv].host.ip + "/",
    lang: ["en","th"]
};