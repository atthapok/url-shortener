'use strict';

const cfg = require('../config/config'),
    fs = require('fs'),
    locale = {};

locale["en"] = JSON.parse(fs.readFileSync(cfg.localize.en, 'utf8'));
locale["th"] = JSON.parse(fs.readFileSync(cfg.localize.th, 'utf8'));

let message = {
    getMsg:  (code, lang) => {
        lang = checkLanguage(lang);
        return {code: code, description: locale[lang].response[code.toString()]};
    },
    getLocale: (lang) => {
        lang = checkLanguage(lang);
        return locale[lang];
    }
};

let checkLanguage = (lang) => {
    return (lang && cfg.lang.indexOf(lang) > -1) ? lang : cfg.localize.default;
};


module.exports = message;
