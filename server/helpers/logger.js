'use strict';

const fs = require('fs'),
    utils = require('./utils'),
    moment = require('moment'),
    cfg = require('../config/config');

const appendLog = (src, desc) => {
    if (cfg.log.debug) {
        let res = src + '[' + getTransactionTime(1) + '] debug: ' + desc + '\n';
        return res;
    }
    return null;
};

const debug = (message, start) => {
    if (cfg.log.debug) {
        writeDebugLogFile(message, start);
    }
};

const diffTimeInMsec = (startTime, stopTime) => {
    return (stopTime - startTime);
};

const getTransactionTime = (timeFlag) => {
    let now = new Date();
    let year = "" + now.getFullYear();
    let month = "" + (now.getMonth() + 1);
    if (month.length === 1) {
        month = "0" + month;
    }
    let day = "" + now.getDate();
    if (day.length === 1) {
        day = "0" + day;
    }
    let hour = "" + now.getHours();
    if (hour.length === 1) {
        hour = "0" + hour;
    }
    let minute = "" + now.getMinutes();
    if (minute.length === 1) {
        minute = "0" + minute;
    }
    let second = "" + now.getSeconds();
    if (second.length === 1) {
        second = "0" + second;
    }
    let millisec = "" + now.getSeconds();
    if (millisec.length === 1) {
        millisec = "00" + millisec;
    } else if (millisec.length === 2) {
        millisec = "0" + millisec;
    }

    if (timeFlag === 1) {
        return year + month + day + hour + minute + second + millisec;
    } else {
        if (cfg.log.rotateTime) {
            let mins = Math.floor(minute / cfg.log.rotateTime) * cfg.log.rotateTime;
            return year + month + day + hour + mins + '.txt'; //Rotate Log every mins by cfg
        }
        return year + month + day + hour + '.txt';//Rotate Log every 1hr
    }

};

const writeDebugLogFile = (appendLog, start) => {
    let logStream = null;
    let stop = new Date().getTime();

    try {
        logStream = fs.createWriteStream(cfg.log.path + getTransactionTime(2), {'flags': 'a'});
        logStream.end(appendLog + '\n');

    } catch (error) {
        console.log('Write Logfile Error : ' + error);
    }
};

const logger = {
    writeLog: (method, req, startTime, res, err) => {
        // if (cfg.log.debug) {
        let stopTime = utils.getTimeInMsec(),
            request = {};

        request.body = req.body;
        request.params = req.params;
        request.query = req.query;

        let logString = {
            level: 'debug',
            dateTime:  moment().format('YYYY-MM-DD HH:mm:ss'),
            command: method,
            header: req.headers,
            ip: req.ip,
            url: req.url,
            request: request,
            response: res,
            response_time: diffTimeInMsec(startTime, stopTime)
        };

        let code = res.code.toString().substring(0,3);


        if (err) {
            logString.err = err;
        }

        let level = (err || code !== "200") ? 'error' : 'debug';

        logString.level = level;

        if (cfg.log.debug || err) {
            logString = JSON.stringify(logString);
            debug(logString, startTime);
        }

        if (err) {
            console.log(err)
            console.log("[Error]: " + method + " | Cause [" + err + "]")
        }
        // }
    },
    debug: (message, start) => {
        // if (cfg.log.debug) {
            writeDebugLogFile(message, start);
        // }
    },
    information: (tag, startTime, message) => {
        let logString = {
            level: 'info',
            dateTime:  moment().format('YYYY-MM-DD HH:mm:ss'),
            command: tag,
            message: message
        }

        logString = JSON.stringify(logString);
        if (cfg.log.debug){
            debug(logString, startTime);
        }

    },
    error: (tag, startTime, message) => {
        let logString = {
            level: 'error',
            dateTime:  moment().format('YYYY-MM-DD HH:mm:ss'),
            command: tag,
            message: message
        }

        console.log("Error: " + message);

        logString = JSON.stringify(logString);
        debug(logString, startTime);
    }
};


module.exports = logger;
