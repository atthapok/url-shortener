'use strict';

const getTimeInMsec = () => {
    return (new Date().getTime());
};

const setResponse = (command, startTime, status, data, err) => {
    return {command: command, startTime: startTime, status: status, data: data, err: err};
};

module.exports = {getTimeInMsec, setResponse};
