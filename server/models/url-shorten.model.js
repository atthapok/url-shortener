const Schema        = require('mongoose').Schema;
const CategorySchema  = new Schema({
    originalUrl: String,
    urlCode: String,
    shortUrl: String,
    createdDate: { type: Date, default: Date.now },
});
const model = require('mongoose').model('UrlShorten', CategorySchema, 'url-shorten');
const selectField = {originalUrl: 1, urlCode: 1, shortUrl: 1};
module.exports = {model, selectField};