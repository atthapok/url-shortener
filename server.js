'use strict';

const http = require('http'),
    fs = require('fs'),
    https = require('https'),
    app = require('./server/config/express'),
    mongoose = require('mongoose'),
    cfg = require('./server/config/config');

let flagStartServer = false,
    interval,
    server,
    serverHttp;

const port = process.env.PORT || cfg.server.port;

mongoose.Promise = global.Promise;
mongoose.connect(cfg.mongodb.uri, cfg.mongodb.serverOpt);

/**
 * Normalize a port into a number, string, or false.
 */
const normalizePort = (val) => {
    let port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
};

/**
 * Event listener for HTTP server "error" event.
 */

const onError = (error) => {
    console.log(error);
    if (error.syscall !== 'listen') {
        throw error;
    }
    // Started up at port ${port}`
    let bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.log(`Server Start Cannot bind port : ${bind}`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.log(`Server Start Port Already In use : ${bind}`);
            process.exit(1);
            break;
        default:
            throw error;
    }
};

/**
 * Event listener for HTTP server "listening" event.
 */

const onListening = () => {
    let addr = server.address();
    let bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    console.log(`Application start on port: ${addr.port}`);
    flagStartServer = true;
};

const startServer = () => {
    if (cfg.server.protocol === 'https'){
        let options = {
            key: fs.readFileSync(cfg.server.key),
            cert: fs.readFileSync(cfg.server.cert)
        };
        console.log('Server start https');
        server = https.createServer(options, app);
    }

    console.log('Server start http');
    serverHttp = http.createServer(app);
    serverHttp.listen(normalizePort(80));

    server.listen(normalizePort(port));
    server.on('error', onError);
    server.on('listening', onListening);


};

mongoose.connection.on('connected', () => {
    console.log('Mongoose default connection success');
    // logger.debug('----- Connect Mongodb Message[Success] -----', start);
    clearInterval(interval);
    if (!flagStartServer) {
        startServer();
    }
});


mongoose.connection.on('error', (err) => {
    console.log('Mongoose default connection error: ' + err);
    // logger.debug('----- Connect Mongodb Message[' + JSON.stringify(err) + '] -----', start);
    interval = setInterval(() => {
        /* Retry to connect db */
        mongoose.connect(cfg.mongodb.uri, cfg.mongodb.serverOpt);
    }, cfg.mongodb.retryTime);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', () => {
    console.log('Mongoose default connection disconnected');
    // logger.debug('----- Connect Mongodb Message[Disconnect] -----', start);
    interval = setInterval(() => {
        /* Retry to connect db */
        mongoose.connect(cfg.mongodb.uri, cfg.mongodb.serverOpt);
    }, cfg.mongodb.retryTime);
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', () => {
    mongoose.connection.close(() => {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});